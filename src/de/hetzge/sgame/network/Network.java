package de.hetzge.sgame.network;

import java.io.Serializable;

public class Network implements IF_Network {

	private final E_NetworkRole networkRole;
	private final IF_Dispatch dispatcher;

	private IF_Network network = null;

	public Network(E_NetworkRole networkRole, IF_Dispatch dispatcher) {
		this.dispatcher = dispatcher;
		this.networkRole = networkRole;
	}

	public void connect(String host, int port) {
		if (this.networkRole == E_NetworkRole.CLIENT) {
			this.network = new TcpClient(this.dispatcher).connect(host, port);
		} else if (this.networkRole == E_NetworkRole.HOST) {
			this.network = new TcpServer(this.dispatcher, port).start();
		} else {
			throw new IllegalStateException("Illegal network role: " + this.networkRole);
		}
	}

	@Override
	public void send(Serializable object) {
		this.network.send(object);
	}

	public void sendAndSelf(Serializable object) {
		send(object);
		this.dispatcher.dispatch(object, null);
	}

	public void sendOrSelf(Serializable object) {
		if (this.networkRole == E_NetworkRole.CLIENT) {
			send(object);
		} else if (this.networkRole == E_NetworkRole.HOST) {
			this.dispatcher.dispatch(object, null);
		} else {
			throw new IllegalStateException("Illegal network role: " + this.networkRole);
		}
	}

	@Override
	public void disconnect() {
		this.network.disconnect();
		this.network = null;
	}

	public boolean isConnected() {
		return this.network != null;
	}

	public E_NetworkRole getNetworkRole() {
		return this.networkRole;
	}
}
