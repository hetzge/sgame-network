package de.hetzge.sgame.network;

public class NetworkDemo {

	public static void main(String[] args) {
		Network network = new Network(E_NetworkRole.valueOf(args[0]), (message, socket) -> {
			System.out.println(String.format("Message received: %s", message));
		});
		network.connect("127.0.0.1", 12345);
		network.send("Hello World");
	}

}
