package de.hetzge.sgame.network;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

import org.nustaq.net.TCPObjectSocket;
import org.nustaq.serialization.FSTConfiguration;
import org.pmw.tinylog.Logger;

class TcpClient implements IF_Network {

	private final IF_Dispatch dispatcher;
	private TCPObjectSocket tcpObjectSocket;

	public TcpClient(IF_Dispatch dispatcher) {
		this.dispatcher = dispatcher;
	}

	public TcpClient connect(String host, int port) {
		try {
			Socket socket = new Socket(host, port);
			FSTConfiguration fstConfiguration = FSTConfiguration.getDefaultConfiguration();
			this.tcpObjectSocket = new TCPObjectSocket(socket, fstConfiguration);
			new AcceptMessageThread(this.tcpObjectSocket, this.dispatcher).start();
			Logger.info("Connected as client");
		} catch (IOException e) {
			Logger.error(e, "error while connecting to server");
		}
		return this;
	}

	@Override
	public void send(Serializable object) {
		NetworkFunction.send(this.tcpObjectSocket, object);
	}

	@Override
	public void disconnect() {
		try {
			this.tcpObjectSocket.close();
		} catch (IOException e) {
			Logger.error(e, "error while disconnect");
		}
	}

}
