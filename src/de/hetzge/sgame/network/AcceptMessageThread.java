package de.hetzge.sgame.network;

import org.nustaq.net.TCPObjectSocket;
import org.pmw.tinylog.Logger;

public class AcceptMessageThread extends Thread {

	private final TCPObjectSocket tcpObjectSocket;
	private final IF_Dispatch dispatcher;

	public AcceptMessageThread(TCPObjectSocket tcpObjectSocket, IF_Dispatch dispatcher) {
		super("AcceptMessageThread");
		this.tcpObjectSocket = tcpObjectSocket;
		this.dispatcher = dispatcher;
	}

	@Override
	public void run() {
		Logger.info("start accepting message thread");

		boolean running = true;
		while (running) {
			try {
				Object object = this.tcpObjectSocket.readObject();
				this.dispatcher.dispatch(object, this.tcpObjectSocket);
			} catch (Exception e) {
				Logger.error(e, "error while read object");
			}
		}
	}

}